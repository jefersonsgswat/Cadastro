import React, { Component, Fragment } from 'react';
import { SiteHeader } from './containers/Header';
import  CadastroPai from './containers/CadastroPai';
import CadastroFilho from './containers/CadastroFilho';

import './App.css';

class App extends Component {
  render() {
    return (
      <Fragment>
        <SiteHeader />
        <CadastroPai />
        <CadastroFilho/>
      </Fragment>
    );
  }
}

export default App;
