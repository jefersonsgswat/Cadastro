import styled from 'styled-components';

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    max-width: 500px;
    width: 90%;
    padding: 30px 10px;
    margin: 100px auto;
    background: #ffffff;
    box-shadow: 0 0 8px 0 rgba(0,0,0,.3);
    border-radius: 5px;
`

export const Label = styled.label`
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: 90%;
    margin-bottom: 10px
`

export const Input = styled.input`
    max-width: 100%;
    padding: 10px;
    border: 1px solid rgba(0,0,0,.3);
    border-radius: 3px;
    margin-top: 10px
`

export const Select = styled.select`
    max-width: 100%;
    padding: 10px;
    border: 1px solid rgba(0,0,0,.3);
    border-radius: 3px;
    margin-top: 10px;
    align-self: flex-start;
`

export const FormButton = styled.button`
    margin: 20px  0 0 10px;
    padding: 10px 15px;
    background: ${props => props.CTA ? 'rgba(255, 99, 71, 1)' : null } ;
    border: 1px solid rgba(0,0,0,.1);
    border-bottom: 2px solid rgba(0,0,0,.3);
    border-radius: 5px;
    font-size: 16px;
    font-weight: 500;
    cursor: pointer;
    align-self: flex-end;
    transition: all .3s ease;
    &:hover {
        background: ${props => props.CTA ? 'rgba(255, 99, 71, .8)' : null };
    }
`

export const FormButtonContainer = styled.div`
    display: flex;
    justify-content: flex-end;
    width: 90%;
    margin-bottom: 10px
`

export const FormTitle = styled.h1`
    margin: 0 auto 20px;
`
