import styled from 'styled-components';

export const HeaderContainer = styled.header`
    width: 100%;
    background: #ffffff;
    box-shadow: 0 0 8px 0 rgba(0,0,0,.3)
`

export const Header = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    max-width: 1000px;
    height: 70px;
    margin: auto;
`
