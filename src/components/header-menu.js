import styled from 'styled-components';

export const HeaderMenu = styled.ul`
    margin: 0;
`

export const MenuItem = styled.li`
    display: inline;
    list-style: none;
    margin-left: 15px;
`
