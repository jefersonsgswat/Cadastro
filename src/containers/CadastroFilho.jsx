import React, { Component } from 'react';
import { Form, Label, Input, Select, FormButton, FormButtonContainer, FormTitle } from '../components/form';

export default class CadastroPai extends Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);

        this.state = {
            nome: '',
            data: '',
            grau: '',
            genero: '',
            apelido: ''
        }
    }

    onChange(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <Form onSubmit={e => e.preventDefault()}>
                <FormTitle>Cadastrar Filho(a)</FormTitle>
                <Label htmlFor='nome'>
                    Nome:
                    <Input name='nome' value={this.state.nome} onChange={this.onChange}></Input>
                </Label>
                <Label htmlFor='genero'>
                    Gênero:
                <Select name='genero' onChange={this.onChange} value={this.state.genero}>
                        <option value='Masculino' >Masculino</option>
                        <option value='Feminino'>Feminino</option>
                    </Select>
                </Label>
                <Label htmlFor='data'>
                    Data de nascimento:
                    <Input name='data' value={this.state.data} onChange={this.onChange}></Input>
                </Label>
                <Label htmlFor='grau'>
                    Grau de parentesco:
                    <Input name='grau' value={this.state.grau} onChange={this.onChange}></Input>
                </Label>
                <Label htmlFor='apelido'>
                    Como você é chamado por ele(a)?:
                    <Input name='apelido' value={this.state.apelido} onChange={this.onChange}></Input>
                </Label>
                <FormButtonContainer>
                    <FormButton CTA >Cadastrar</FormButton>
                    <FormButton>Cancelar</FormButton>
                </FormButtonContainer>
            </Form>
        )
    }
}
