import React from 'react';
import { HeaderContainer, Header } from '../components/header';
import { Logo } from '../components/logo';
import { HeaderMenu, MenuItem } from '../components/header-menu';

import logo from '../logo.svg';

export const SiteHeader = props => {
    return (
        <HeaderContainer>
            <Header>
                <Logo src={logo}></Logo>
                <HeaderMenu>
                    <MenuItem>O que é</MenuItem>
                    <MenuItem>Como funciona</MenuItem>
                    <MenuItem>Quero usar</MenuItem>
                    <MenuItem>Quem somos</MenuItem>
                    <MenuItem>Contato</MenuItem>
                </HeaderMenu>
            </Header>
        </HeaderContainer>
    )
}
