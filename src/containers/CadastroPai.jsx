import React, { Component } from 'react';
import { Form, Label, Input, FormButton, FormButtonContainer, FormTitle } from '../components/form';

export default class CadastroPai extends Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);

        this.state = {
            nome: '',
            email: '',
            senha: '',
            telefone: ''
        }
    }

    onChange(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <Form onSubmit={e => e.preventDefault()}>
                <FormTitle>Cadastro</FormTitle>
                <Label htmlFor='nome'>
                    Nome:
                    <Input name='nome' value={this.state.nome} onChange={this.onChange}></Input>
                </Label>
                <Label htmlFor='email'>
                    Email:
                    <Input type='email' name='email' value={this.state.email} onChange={this.onChange}></Input>
                </Label>
                <Label htmlFor='senha'>
                    Senha:
                    <Input type='password' name='senha' value={this.state.senha} onChange={this.onChange}></Input>
                </Label>
                <Label htmlFor='telefone'>
                    Telefone(opcional):
                    <Input type='tel' name='telefone' value={this.state.telefone} onChange={this.onChange}></Input>
                </Label>
                <FormButtonContainer>
                    <FormButton CTA >Cadastrar</FormButton>
                </FormButtonContainer>
            </Form>
        )
    }
}
